import java.util.HashMap;

public class Main {

    private static HashMap<Integer, Long> cache = new HashMap<>();

    // Recursion
    public static long fibonacciRecursivo(int n){
        if (n <= 1){
            return n;
        }
        return fibonacciRecursivo(n - 1) + fibonacciRecursivo(n - 2);
    }


    // Dinamica
    public static long fibonacciDinamico(int n){
        if (n <= 1){
            return n;
        }

        long[] fibonacci_aux =  new long[n + 1];
        fibonacci_aux[1] = 1;

        for (int i = 2; i <= n; i++){
            fibonacci_aux[i] = fibonacci_aux[i-1] + fibonacci_aux[i - 2];
        }

        return fibonacci_aux[n];

    }


    // Memoization
    public static long fibonacciMemoization(int n){

        if (cache.containsKey(n)){
            return cache.get(n);
        }

        if (n <= 1){
            cache.put(n, (long) n);
            return n;
        }


        long result = fibonacciRecursivo(n - 1) + fibonacciRecursivo(n - 2);
        cache.put(n, result);

        return result;

    }


    public static void main(String[] args) {

        int n = 40;
        System.out.println("Fibonacci Recursivo ("+ n +") = " + fibonacciRecursivo(n));
        System.out.println("Fibonacci Dinamico ("+ n +") = " + fibonacciDinamico(n));
        System.out.println("Fibonacci Memoization ("+ n +") = " + fibonacciMemoization(n));
    }
}